#ifndef FLAME_SERVER_H
#define FLAME_SERVER_H

#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

# include <pthread.h>

#include "config.h"
#include "operations.h"
#include "operations/echo.h"
#include "operations/matmul.h"
#include "operations/stream.h"

struct Server
{
  int socket;
  int client_socket;
  struct sockaddr_in addr;
  struct sockaddr_in client_addr;
};

Status
server(Config* config);

#endif // FLAME_SERVER_H
