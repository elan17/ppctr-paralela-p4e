
# ifndef memoization_h
# define memoization_h

# include <pthread.h>


struct returneoBusqueda{
    int clave;
    pthread_mutex_t* mutex;
    void* matrix;
};

struct ParMatMul{
    int clave;
    void* valor;
};

struct CacheMatMul{
    int tamano;
    int pos;
    pthread_mutex_t* mutexes;
    ParMatMul* buffer;
    bool* inicializado;
    pthread_mutex_t escritura;
};

void inicializaCacheMatMul(CacheMatMul* cache, int tamano);
void anadeValor(CacheMatMul* cache, ParMatMul pareja);
void liberaCacheMatMul(CacheMatMul* cache);
returneoBusqueda obten_pareja_aletoria(CacheMatMul* cache);
returneoBusqueda buscaValor(CacheMatMul* cache, int clave);

# endif
