# ifndef memoization_c
# define memoization_c

# include "memoization.h"
# include <pthread.h>
# include <stdlib.h>
# include <time.h>

void inicializaCacheMatMul(CacheMatMul* cache, int tamano){
    cache->tamano=tamano;
    int Cache_Size=tamano;
    cache->pos = 0;
    cache->mutexes=(pthread_mutex_t*)malloc(Cache_Size*sizeof(pthread_mutex_t));
    cache->buffer=(ParMatMul*)malloc(Cache_Size*sizeof(ParMatMul));
    cache->inicializado=(bool*)malloc(Cache_Size*sizeof(bool));
    cache->escritura=PTHREAD_MUTEX_INITIALIZER;
    for(int i=0; i<Cache_Size; i++){
        cache->mutexes[i]=PTHREAD_MUTEX_INITIALIZER;
        cache->buffer[i].clave=-1;
        cache->inicializado[i]=false;
    }
    pthread_mutex_unlock(&cache->escritura);
}

returneoBusqueda obten_pareja_aletoria(CacheMatMul* cache){
    int inicializados=0;
    int* indices_permitidos = (int*)malloc(cache->tamano*sizeof(int));
    
    /* Esto es lo más ineficiente del mundo, lo lógico sería hacer una
     * variable condicional y que cada vez que alguien libere un mutex
     * mandara un signal. Sin embargo, el while es un edgecase ya que
     * la selección de ataque va a ser una sección crítica y el tamaño de la
     * cache va superar con creces el número de threads*/
    while(inicializados==0){
        for(int i=0; i<cache->tamano; i++){
            if(cache->inicializado[i] == true 
                    && pthread_mutex_trylock(&cache->mutexes[i])){
                indices_permitidos[inicializados]=i;
                inicializados++;
            }
            else{
                break;
            }
        }
        if(inicializados==0){
            sleep(0.1);
        }
    }
    int eleccion = RAND%inicializados;
    int eleccionFin = indices_permitidos[eleccion];
    for(int i=0; i<inicializados; i++){
        if(i!=eleccion){
            int index = indices_permitidos[i];
            pthread_mutex_unlock(&cache->mutexes[i]);
        }
    }

    returneoBusqueda returneo;
    returneo.clave = cache->buffer[eleccionFin].clave;
    returneo.mutex = &cache->mutexes[eleccionFin];
    returneo.matrix = cache->buffer[eleccionFin].valor;
    return returneo;
}

/**
 * Calling while threads are operating is undefined
 */
void liberaCacheMatMul(CacheMatMul* cache){ 
    free(cache->mutexes);
    free(cache->buffer);
    free(cache->inicializado);
}

void anadeValor(CacheMatMul* cache, ParMatMul pareja){

    int Cache_Size = cache->tamano; 
    int pos = cache->pos;
    
    pthread_mutex_lock (&cache->escritura);
    
    while(!pthread_mutex_trylock(&cache->mutexes[pos])){
        pos = (pos+1)%Cache_Size;
    }

    if(cache->inicializado[pos]){
        free(cache->buffer[pos].valor);
    }
    else{
        cache->inicializado[pos] = true;
    }
    cache->buffer[pos] = pareja;
    
    cache->pos = (pos+1)%Cache_Size;
    
    pthread_mutex_unlock(&cache->mutexes[pos]);
    pthread_mutex_unlock(&cache->escritura);
}

returneoBusqueda buscaValor(CacheMatMul* cache, int clave){
    int Cache_Size = cache->tamano;
    returneoBusqueda returneo;
    for(int i=0; i<Cache_Size; i++){
        pthread_mutex_lock(&cache->mutexes[i]);
        returneo.matrix = cache->buffer[i].valor;
        returneo.mutex = &cache->mutexes[i];
        if(cache->inicializado[i] && cache->buffer[i].clave == clave){
            return returneo;
        }
        pthread_mutex_unlock(&cache->mutexes[i]);
    }
    returneo.matrix=NULL;
    return returneo;
}

# endif
