#include "flame.h"

#include "./lib/memoization.h"

int
main(int argc, char* argv[])
{
  Config config;
  config_init(&config);
  Status st = config_parser(argc, argv, &config);
  if (st != Status::Ok) {
    return st;
  }
  if (config.debug) {
    config_print(&config);
  }


  if (config.mode == Mode::Server) {
    server(&config);
  } else {
    client(&config);
  }
  return Status::Ok;
}
