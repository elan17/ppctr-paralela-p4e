# include "../client.h"
# include "../config.h"


Status send_matmul(Config* config, Client client){
     
      unsigned int dim;
      //returneoBusqueda peticion = obten_pareja_aletoria(&config->cache);

      dim = 83;
      //dim = peticion.clave;
      int expected_size = 8 + dim * dim * sizeof(float); // 8 header
      char buffer[expected_size];
      
      matmul_init(buffer, dim);

      if (send(client.socket, buffer, strlen(buffer) + 1, 0) < 0) {
        puts("[error] send");
        return Status::Err;
      }
      if (config->debug) {
        printf("[done] send: '%s'\n", buffer);
      }

      char* ptr = (char*)&buffer;

      int size = 0;
      int chunks = 0;
      int read_size = 0;
      do {
        ptr = (ptr + read_size);
        read_size = recv(client.socket, ptr, 2000, 0);
        if (read_size < 0) {
          puts("[error] recv");
          return Status::Err;
        } else {
          chunks++;
          size += read_size;
        }
      } while (read_size > 0 && size < expected_size);
      if (config->debug) {
        printf("====recv==== [%dB %d chunk(s)] matmul: C = A*B:\n", size, chunks);
        float* C = matmul_parse_response(buffer);
        if (C == nullptr) {
          puts("[error] matmul response");
        } 
        //else if(!equal_matrixes(C, (float*)peticion.matrix, dim)){
          //puts("[error] matmul response incorrect");
          //matmul_matrix_print(C, dim);
          //matmul_matrix_print((float*)peticion.matrix, dim);
          //exit(-1);
        //} 
        printf("============\n");
      }
      //pthread_mutex_unlock(peticion.mutex);
      return Status::Ok;
}

int max_dimension(Config* config){
    return 100;
}

Status fill_cache(Config* config){
    while(true){
        int max_dim=max_dimension(config);
        int dim = rand()%max_dim;
        float** Ms = matmul_alloc(dim);
        matmul_fill(Ms, dim);
        matmul_compute(Ms, dim);
        matmul_destroy(Ms);

        ParMatMul par;
        par.clave = dim;
        par.valor = Ms[2];
        anadeValor(&config->cache, par);
    }
}


