# include "../client.h"
# include "../config.h"


Status send_stream(Config* config, Client client){
    
      int expected_size = 8 + 40 * sizeof(char); // 8 header
      char buffer[expected_size];

      stream_init(buffer);

      if (send(client.socket, buffer, strlen(buffer) + 1, 0) < 0) {
        puts("[error] send");
        return Status::Err;
      }
      if (config->debug) {
        printf("[done] send: '%s'\n", buffer);
      }

      const char* message1 =
        "El ecologismo (en ocasiones llamado el movimiento verde o ambientalista) ";
      const char* message2 = "es un variado movimiento político, social y global, que defiende la "
                             "protección del medio ambiente. ";
      const char* message3 =
        "Recomiendo la película 'Barbacana, la huella del lobo', recientemente proyectada en cines "
        "y rodada durante 12 años. Refleja el conflicto por la conservación del lobo ibérico y "
        "pone de manifiesto la ineficacia de las medidas actuales.";

      if (send(client.socket, message1, strlen(message1), 0) < 0) {
        puts("[error] send");
        return Status::Err;
      }
      if (config->debug) {
        printf("[done] send: '%s'\n", message1);
      }
      if (send(client.socket, message2, strlen(message2), 0) < 0) {
        puts("[error] send");
        return Status::Err;
      }
      if (config->debug) {
        printf("[done] send: '%s'\n", message2);
      }
      if (send(client.socket, message3, strlen(message3) + 1, 0) < 0) { // \0 sent
        puts("[error] send");
        return Status::Err;
      }
      if (config->debug) {
        printf("[done] send: '%s'\n", message3);
      }

      char* ptr = (char*)&buffer;

      int size = 0;
      int chunks = 0;
      int read_size = 0;
      do {
        ptr = (ptr + read_size);
        read_size = recv(client.socket, ptr, 2000, 0);
        if (read_size < 0) {
          puts("[error] recv");
          return Status::Err;
        } else {
          chunks++;
          size += read_size;
        }
      } while (read_size > 0 && size < expected_size);
      if (config->debug) {
        printf("====recv==== [%dB %d chunk(s)] stream: B = fx(A):\n", size, chunks);
        ptr = stream_parse_response(buffer);
        puts(ptr);
        printf("============\n");
      }
      return Status::Ok;
}



