# include "../operations/echo.h"
# include "../config.h"
# include "../client.h"

/**
 * @private
 **/
Status
client_reconfig_echo(Client* client, Config* config)
{
  Status st = Status::Ok;

  client->timeout.tv_sec = 0;
  client->timeout.tv_usec = 700000;

  if (setsockopt(
        client->socket, SOL_SOCKET, SO_RCVTIMEO, (char*)&client->timeout, sizeof(client->timeout)) <
      0) {
    perror("[error] setsockopt");
    return Status::Err;
  }

  return st;
}


Status send_echo(Config* config, Client client){
      int expected_size = 5+4;
      char buffer[expected_size];
      Status st = client_reconfig_echo(&client, config);
      if (st != Status::Ok) {
        return st;
      }

      int offset = rand()%998;
      long init_value = 1000+offset;
      echo_init(buffer, init_value);

      if (send(client.socket, buffer, strlen(buffer) + 1, 0) < 0) {
        puts("[error] send");
        return Status::Err;
      }
      if (config->debug) {
        printf("[done] send: '%s'\n", buffer);
      }

      char* ptr = (char*)&buffer;

      int size = 0;
      int chunks = 0;
      int read_size = 0;
      do {
        ptr = (ptr + read_size);
        read_size = recv(client.socket, ptr, 2000, 0);
        if (read_size < 0) {
          if (errno != ETIMEDOUT && errno != EAGAIN) {
            puts("[error] recv");
            return Status::Err;
          }
          if (config->debug) {
            puts("[info] recv timeout");
          }
        } else {
          chunks++;
          size += read_size;
        }
      } while (read_size > 0 && size < expected_size);
      long value = echo_parse_response(buffer);
      if (value == init_value + 1) {
        if (config->debug) {
          printf("====recv==== [%dB]\n", size);
          printf("%ld", value);
          printf("%s============\n", (size > 0 ? "\n" : ""));
        }
      } else {
        if (config->debug) {
          printf("[echo] wrong response: %d\n", value);
        }
      }
    return st;
}
