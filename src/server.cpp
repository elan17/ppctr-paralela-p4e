#include "server.h"
#include "./server/receiver.c"
#include <omp.h>

int open_sockets=0;

/**
 * @private
 **/
Status
server_create(Server* server, Config* config)
{
  Status st = Status::Ok;

  server->addr.sin_family = AF_INET;
  server->addr.sin_addr.s_addr = inet_addr(config->host);
  server->addr.sin_port = htons(config->port);

  int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (socket_fd == -1) {
    perror("[error] socket");
    return Status::Err;
  }
  if (config->debug) {
    puts("[done] socket");
  }

  if (bind(socket_fd, (const sockaddr*)&(server->addr), sizeof(server->addr)) < 0) {
    perror("[error] bind");
    return Status::Err;
  }
  if (config->debug) {
    puts("[done] bind");
  }

  server->socket = socket_fd;

  return st;
}

/**
 * @private
 **/
Status
server_listen(Server* server, Config* config)
{
  if (listen(server->socket, 3) != 0) {
    perror("[error] listen");
    return Status::Err;
  }
  if (config->debug) {
    puts("[done] listen");
  }
  return Status::Ok;
}

/**
 * @private
 **/
Status
server_accept(Server* server, Config* config)
{
  if (config->debug) {
    puts("[waiting] accept");
  }
  int c = sizeof(struct sockaddr_in);
  int socket_fd = accept(server->socket, (struct sockaddr*)&server->client_addr, (socklen_t*)&c);
  if (socket_fd < 0) {
    perror("[error] accept");
    return Status::Err;
  }
  if (config->debug) {
    puts("[done] accept");
  }

  int reuse = 1;

  if (setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) < 0) {
    perror("[error] setsockopt");
    return Status::Err;
  }
  #pragma omp atomic
  open_sockets++;

  server->client_socket = socket_fd;

  return Status::Ok;
}

Status
server(Config* config)
{   
        inicializaCacheMatMul(&config->cache, 10);
        Status st = Status::Ok;
        Server server;
        unsigned int connections = config->connections;
        st = server_create(&server, config);
        if (st != Status::Ok) {
            return st;
        }

        st = server_listen(&server, config);
        if (st != Status::Ok) {
            return st;
        }

        Protocol protocol = config->protocol;

        bool cont = true;
        int times = 0;
  omp_set_nested(1);
  # pragma omp parallel shared(server, config)
  {
    # pragma omp single 
    {
        while (cont) {
        
            st = server_accept(&server, config); 

            if(st!=Status::Ok){
                break;
            }

            int read_size;

            if (config->debug) {
                puts("[waiting] recv");
            }
            
            # pragma omp task
            {
                handler(server, config, operation_from_protocol(protocol));
                printf("[info] client disconnected: %i peticiones activas\n", open_sockets);
                if (read_size == 0) {
                    if(config->debug)
                        printf("[info] client disconnected: %i peticiones activas\n", open_sockets);
                    # pragma omp atomic 
                    open_sockets--;
                    close(server.client_socket);
                    fflush(stdout);
                } else if (read_size == -1) {
                    perror("[error] recv");
                }
            }

            /* if connections == 0, loop forever */
            if (connections) {
            times++;
                if (times >= connections) {
                    cont = false;
                }
            }
        }
        if (config->debug) {
            puts("[exit]");
        }
    }
    # pragma omp taskwait
  }
  liberaCacheMatMul(&config->cache);
  return st;
}
