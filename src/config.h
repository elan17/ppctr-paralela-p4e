#ifndef FLAME_PARSER_H
#define FLAME_PARSER_H

#include <stdlib.h>
#include <string.h>

# include "./lib/memoization.h"

# include "operations.h"

#define MAX_CONNECTIONS 2

enum class Mode
{
  Server,
  Client,
};

enum class Preset
{
  Invalid = 0,
  Generic = 1,
  DiegoVictor = 2,
  Juan = 3,
  Marina = 4,
  David = 5,
  EnriqueEmilio = 6,
  DanielBrian = 7,
  DavidAxiel = 8,
  DanielRaul = 9,
  IvanAlejandro = 10,
};

enum class Protocol
{
  Invalid = 0,
  Echo = 1,
  MatMul = 2,
  Stream = 3,
  Mixed = 4,
};

struct Config
{
  Mode mode;
  int port;
  const char* host;
  Preset preset;
  Protocol protocol;
  CacheMatMul cache;
  bool debug;
  unsigned int connections;
};

enum Status
{
  Ok = 0,
  Err = 1,
};

Status
config_parser(int argc, char* argv[], Config* config);

void
config_print(Config* config);

void
config_init(Config* config);

const char*
config_str_preset(Preset preset);

Preset
config_preset(const char* str_preset);

void
config_print_presets();

const char*
config_str_protocol(Protocol protocol);

Protocol
config_protocol(const char* str_protocol);

void
config_print_protocols();

Operation
operation_from_protocol(Protocol protocol);

#endif // FLAME_PARSER_H
