#ifndef FLAME_OPERATIONS_MATMUL_H
#define FLAME_OPERATIONS_MATMUL_H

#include <cstdio>
#include <cstdlib>
#include <cstring>

#define RAND (rand() % 3)
#define MAX_DIM 65535

void
matmul_init(char* buffer, unsigned int dim);
int
matmul_parse(char* buffer);
unsigned int
matmul_init_response(char* buffer);

float*
matmul_parse_response(char* buffer);

float**
matmul_alloc(int dim);
float*
matmul_matrix_alloc(int dim);
void
matmul_destroy(float** Ms);

void
matmul_fill(float** Ms, int dim);
void
matmul_matrix_fill(float* M, int dim);
void
matmul_matrix_print(float* M, int dim);
void
matmul_compute(float** Ms, int dim);
bool
equal_matrixes(float* A, float* B, int dim);
#endif // FLAME_OPERATIONS_MATMUL_H
