#include "echo.h"

void
echo_init(char* buffer, long value)
{
  sprintf(buffer, "op:%s:%ld\0", "echo", value);
}

long
echo_parse(char* buffer)
{
  long val;
  int n = sscanf(buffer, "op:echo:%ld", &val);
  return val;
}

void
echo_init_response(char* buffer, long value)
{
  sprintf(buffer, "%s:%ld\0", "echo", value + 1);
}

long
echo_parse_response(char* buffer)
{
  if (strncmp(buffer, "echo:", 5) != 0) {
    return 0;
  }
  long val;
  int n = sscanf(buffer, "echo:%ld", &val);
  return val;
}
