# include "client.h"
# include "client/echoHandler.c"
# include "client/matmulHandler.c"
# include "client/streamHandler.c"
# include <omp.h>

/**
 * @private
 **/
Status
client_create(Client* client, Config* config)
{
  Status st = Status::Ok;

  int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (socket_fd == -1) {
    perror("[error] socket");
    return Status::Err;
  }
  if (config->debug) {
    puts("[done] socket");
  }

  client->addr.sin_addr.s_addr = inet_addr(config->host);
  client->addr.sin_family = AF_INET;
  client->addr.sin_port = htons(config->port);

  client->timeout.tv_sec = 3;
  client->timeout.tv_usec = 0;

  if (setsockopt(
        socket_fd, SOL_SOCKET, SO_RCVTIMEO, (char*)&client->timeout, sizeof(client->timeout)) < 0) {
    perror("[error] setsockopt");
    return Status::Err;
  }

  if (setsockopt(
        socket_fd, SOL_SOCKET, SO_SNDTIMEO, (char*)&client->timeout, sizeof(client->timeout)) < 0) {
    perror("[error] setsockopt");
    return Status::Err;
  }

  client->socket = socket_fd;

  return st;
}


/**
 * @private
 **/
Status
client_connect(Client* client, Config* config)
{
  if (connect(client->socket, (const sockaddr*)&client->addr, sizeof(client->addr)) < 0) {
    perror("[error] connect");
    return Status::Err;
  }
  if (config->debug) {
    puts("[done] connect");
  }
  return Status::Ok;
}

/**
 * @private
 **/
Status
client_disconnect(Client* client, Config* config)
{
  close(client->socket);
  if (config->debug) {
    puts("[done] disconnect");
  }
  return Status::Ok;
}

Status
client(Config* config)
{
  Status st = Status::Ok;

  Protocol protocol = config->protocol;
  Operation operation = operation_from_protocol(protocol);

  inicializaCacheMatMul(&config->cache, 10);

  bool cont = true;
  int times = 0;
  unsigned int connections = config->connections;

  omp_set_nested(1);
  # pragma omp parallel
  {
  # pragma omp single
  {
    if(operation == Operation::MatMul){
        //# pragma omp task
        //fill_cache(config);
    }
    while (true) {
      # pragma omp task
      {
      Client client;
      st = client_create(&client, config);
      if (st != Status::Ok) {
        //return st;
      }

      st = client_connect(&client, config);
      if (st != Status::Ok) {
        //return st;
      }
      if (operation == Operation::Echo) {
        send_echo(config, client); 
      } else if (operation == Operation::MatMul) {
        send_matmul(config, client); 
      } else if (operation == Operation::Stream) {
        send_stream(config, client);
      } else {
        puts("[cmd] invalid operation");
      }
      st = client_disconnect(&client, config);
      if (st != Status::Ok) {
        //return st;
      }
      }
      /* if connections == 0, loop forever */  
      if (connections) {
        times++;
        if (times >= connections) {
            cont = false;
        }
      }
    }
    if (config->debug) {
        puts("[exit]");
    }
  }
  }
  return Status::Ok;
}
