#include "config.h"
#include <getopt.h>
#include <stdio.h>

#define CONFIG_PORT_DEFAULT 8000
#define CONFIG_CONNECTIONS_DEFAULT MAX_CONNECTIONS
#define CONFIG_HOST_DEFAULT "127.0.0.1"
#define CONFIG_PRESET_DEFAULT "Generic"
#define CONFIG_PROTOCOL_DEFAULT "Echo"

void
config_init(Config* config)
{
  config->mode = Mode::Server;
  config->port = CONFIG_PORT_DEFAULT;
  config->host = CONFIG_HOST_DEFAULT;
  config->preset = config_preset(CONFIG_PRESET_DEFAULT);
  config->protocol = config_protocol(CONFIG_PROTOCOL_DEFAULT);
  config->debug = false;
  config->connections = 0;
}

void
config_print(Config* config)
{
  printf("Config {\n"
         "  mode: %s\n"
         "  host: %s\n"
         "  port: %d\n"
         "  preset: %s\n"
         "  protocol: %s\n"
         "  connections: %d\n"
         "  debug: %s\n"
         "}\n",
         (config->mode == Mode::Server ? "server" : "client"),
         config->host,
         config->port,
         config_str_preset(config->preset),
         config_str_protocol(config->protocol),
         config->connections,
         (config->debug ? "yes" : "no"));
}

const char* g_str_presets[] = {
  "Invalid",       "Generic",     "DiegoVictor", "Juan",       "Marina",        "David",
  "EnriqueEmilio", "DanielBrian", "DavidAxiel",  "DanielRaul", "IvanAlejandro",
};

const char* g_str_protocols[] = {
  "Invalid", "Echo", "MatMul", "Stream", "Mixed",
};

const char*
config_str_preset(Preset preset)
{
  return g_str_presets[static_cast<int>(preset)];
}

Preset
config_preset(const char* str_preset)
{
  int lstr_presets = sizeof(g_str_presets) / sizeof(char*);
  int sel = 0;
  for (int i = 0; i < lstr_presets; ++i) {
    if (strcmp(g_str_presets[i], str_preset) == 0) {
      sel = i;
      break;
    }
  }
  return static_cast<Preset>(sel);
}

void
config_print_presets()
{
  printf("Presets:\n");
  int lstr_presets = sizeof(g_str_presets) / sizeof(char*);
  for (int i = 1; i < lstr_presets; ++i) {
    printf("  %s\n", g_str_presets[i]);
  }
  printf("\n");
}

const char*
config_str_protocol(Protocol protocol)
{
  return g_str_protocols[static_cast<int>(protocol)];
}

Protocol
config_protocol(const char* str_protocol)
{
  int lstr_protocols = sizeof(g_str_protocols) / sizeof(char*);
  int sel = 0;
  for (int i = 0; i < lstr_protocols; ++i) {
    if (strcmp(g_str_protocols[i], str_protocol) == 0) {
      sel = i;
      break;
    }
  }
  return static_cast<Protocol>(sel);
}

void
config_print_protocols()
{
  printf("Protocols:\n");
  int lstr_protocols = sizeof(g_str_protocols) / sizeof(char*);
  for (int i = 1; i < lstr_protocols; ++i) {
    printf("  %s\n", g_str_protocols[i]);
  }
  printf("\n");
}

Operation
operation_from_protocol(Protocol protocol)
{
  Operation operation = Operation::Echo;
  switch (protocol) {
    case Protocol::MatMul:
      operation = Operation::MatMul;
      break;
    case Protocol::Stream:
      operation = Operation::Stream;
      break;
    case Protocol::Mixed:
      operation = Operation::Invalid; 
      break;
  }
  return operation;
}

Status
config_parser(int argc, char* argv[], Config* config)
{
  Status ret = Status::Ok;
  int c;
  const char* short_opt = "CP:H:hp:s:d";
  struct option long_opt[] = { { "client", no_argument, NULL, 'C' },
                               { "port", required_argument, NULL, 'P' },
                               { "host", required_argument, NULL, 'H' },
                               { "preset", required_argument, NULL, 's' },
                               { "connections", required_argument, NULL, 'c' },
                               { "protocol", required_argument, NULL, 'p' },
                               { "help", no_argument, NULL, 'h' },
                               { "debug", no_argument, NULL, 'd' },
                               { NULL, 0, NULL, 0 } };

  while ((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1) {
    switch (c) {
      case -1: /* no more arguments */
      case 0:  /* long options toggles */
        break;

      case 'C':
        config->mode = Mode::Client;
        break;

      case 'H':
        config->host = optarg;
        break;

      case 'P':
        config->port = atoi(optarg);
        break;

      case 'p':
        config->protocol = config_protocol(optarg);
        if (config->protocol == Protocol::Invalid) {
          printf("Invalid protocol: %s\n", optarg);
          ret = Status::Err;
        }
        break;

      case 's':
        config->preset = config_preset(optarg);
        if (config->preset == Preset::Invalid) {
          printf("Invalid preset: %s\n", optarg);
          ret = Status::Err;
        }
        break;

      case 'c':
        config->connections = atoi(optarg);
        break;

      case 'd':
        config->debug = true;
        break;

      case 'h':
        printf("Usage: %s [Options]\n"
               "\n"
               "Options:\n"
               "  -C, --client                     mode client (default: server)\n"
               "  -H, --host         <host>        host (default: %s)\n"
               "  -P, --port         <port>        port (default: %d)\n"
               "  -s, --preset       <preset>      available presets shown below (default: %s)\n"
               "  -p, --protocol     <protocol>    available protocols shown below (default: %s)\n"
               "  -c, --connections  <number>      max number of tcp connections (default: %d; 0 = "
               "infinite)\n"
               "  -h, --help                       print this help and exit\n"
               "\n",
               argv[0],
               CONFIG_HOST_DEFAULT,
               CONFIG_PORT_DEFAULT,
               CONFIG_PRESET_DEFAULT,
               CONFIG_PROTOCOL_DEFAULT);
        config_print_presets();
        config_print_protocols();
        ret = Status::Err;
        break;

      case ':':
      case '?':
        printf("Try `%s --help' for more information.\n", argv[0]);
        ret = Status::Err;
        break;

      default:
        printf("%s: invalid option -- %c\n", argv[0], c);
        printf("Try `%s --help' for more information.\n", argv[0]);
        ret = Status::Err;
        break;
    };
  }
  return ret;
}
