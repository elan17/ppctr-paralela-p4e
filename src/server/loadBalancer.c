# include "../server.h"
# include <pthread.h>
# include "../lib/memoization.c"
# include "omp.h"

void protocoloEcho(Server server, Config* config, char* buffer){
    if (config->debug) {
        puts("[cmd] echo");
    }
    long val = echo_parse(buffer);
    if (config->debug) {
        printf("[echo] parsed value: '%ld'\n", val);
    }
    echo_init_response(buffer, val);
    if (config->debug) {
        printf("[echo] send message: '%s'\n", buffer);
    }
    write(server.client_socket, buffer, strlen(buffer) + 1);
    free(buffer);
}


void protocoloMatMul(Server server, Config* config, char* buffer){
    if (config->debug) {
        puts("[cmd] matmul");
    }
    unsigned int dim = matmul_parse(buffer) % MAX_DIM;
    if (config->debug) {
        printf("[matmul] parsed dim: '%d'\n", dim);
    }
    unsigned int lbuffer = matmul_init_response(buffer);
    int size = dim * dim * sizeof(float);
    returneoBusqueda valor;
    bool encontrado; 
    valor = buscaValor(&config->cache, dim);
    encontrado = true;
    if(valor.matrix == NULL){
        encontrado = false;
        float** Ms = matmul_alloc(dim);
        valor.matrix = Ms[2];
        if (config->debug) {
            printf("[matmul] filling and computing\n");
        }
        matmul_fill(Ms, dim);
        matmul_compute(Ms, dim);
        if (config->debug) {
            /**
            printf("A:\n");
            matmul_matrix_print(Ms[0], dim);
            printf("B:\n");
            matmul_matrix_print(Ms[1], dim);
            printf("C:\n");
            matmul_matrix_print((float*)valor.matrix, dim);
            */
        }
        matmul_destroy(Ms);

        ParMatMul par;
        par.clave = dim;
        par.valor = valor.matrix;
        anadeValor(&config->cache, par);
    }
    write(server.client_socket, buffer, lbuffer);
    write(server.client_socket, (char*)valor.matrix, size);
    free(buffer);
    if(encontrado)
        pthread_mutex_unlock(valor.mutex);
}


void protocoloStream(Server server, Config* config, char* buffer, int read_size){
    
    if (config->debug) {
        puts("[cmd] stream");
    }
    OperationStream opstream;
    int offset = stream_parse(buffer);
    char* ptr = &buffer[offset];

    read_size -= offset;
    int end;
    bool expect_more = (read_size == 0);
    do {
        end = strnlen(ptr, read_size);
        if (config->debug) {
            printf("[stream] chunk: '");
            fwrite(ptr, 1, end, stdout);
            printf("' [%dB]\n", end);
        }

        if (end > 0) {
            std::string chunk(ptr, end);
            opstream.update(chunk);
            if (config->debug) {
                printf("[stream] chunk updated [%dB]\n", end);
            }
        }

        expect_more = (read_size == 0 || end == read_size); // no end yet
        if (expect_more) {
            if (config->debug){
                printf("[stream] waiting\n");
            }
            read_size = recv(server.client_socket, buffer, 2000, 0);
            offset = 0;
            ptr = buffer;
            if (read_size <= 0) {
                expect_more = false;
            }
        }

    } while (expect_more);
    // streaming: inner value is consumed afterwards
    const char* streamed = strdup(opstream.final().c_str());
    int lstreamed = strlen(streamed);
    if (config->debug) {
        printf("[stream] streamed: '%s'\n", streamed);
    }
    unsigned int lbuffer = stream_init_response(buffer);
    write(server.client_socket, buffer, lbuffer);
    write(server.client_socket, streamed, lstreamed + 1);
    free(buffer);
}
