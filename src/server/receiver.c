# include "../server.h"
# include "./loadBalancer.c"
# include <pthread.h>


void handler(Server server, Config* config, Operation operation){
    char* buffer=(char*)malloc(2000*sizeof(char));
    int read_size;
    
    while ((read_size = recv(server.client_socket, buffer, 2000, 0)) > 0) {
      if (config->debug) {
        printf("====recv==== [%dB]\n", read_size);
        printf("%s", buffer);
        printf("%s============\n", (read_size > 0 ? "\n" : ""));
      }

      if (strncmp("op:", buffer, 3) != 0) {
        puts("[cmd] invalid");
        continue;
      }

      Operation req_op = match_operation(operation, buffer);

      if (req_op == Operation::Echo) {
        protocoloEcho(server, config, buffer);
      } else if (req_op == Operation::MatMul) {
        # pragma omp task
        protocoloMatMul(server, config, buffer); 
      } else if (req_op == Operation::Stream) {
        # pragma omp task
        protocoloStream(server, config, buffer, read_size); 
      } else {
        puts("[cmd] invalid");
      }
      buffer=(char*)malloc(2000*sizeof(char));
    }
    free(buffer);
}













