#include "operations.h"

Operation
match_operation(Operation operation, char* str)
{
  Operation op = operation;
  if ((operation == Operation::Echo || operation == Operation::Invalid)&& strncmp("op:echo:", str, 8) == 0) {
        op = Operation::Echo;
  } else if ((operation == Operation::MatMul || operation == Operation::Invalid)&& strncmp("op:matmul:", str, 10) == 0) {
        op = Operation::MatMul;
  } else if ((operation == Operation::Stream || operation == Operation::Invalid)&& strncmp("op:stream:", str, 10) == 0) {
        op = Operation::Stream;
  } else {
    op = Operation::Invalid;
  }
  return op;
}
